;
/* не удалять */
(function ($) {

	var app = {

		initialize: function () {
			this.setUpListeners();
			this.domReady();
		},

		setUpListeners: function () {
			$('form').on('submit', app.submitForm);
			$('form').on('keydown', 'input', app.actionInputKeydown);
		},

		submitForm: function (e) {
			e.preventDefault();

			var form = $(this),
				submitBtn = form.find('button[type="submit"]');

			if (app.validateForm(form) === false) return false;

			submitBtn.attr('disabled', 'disabled');

			var str = form.serialize();

			$.ajax({
				url: '/mail.php',
				type: 'POST',
				data: str
			})
				.done(function (msg) {
					if (msg === "OK") {

					} else {

					}
				})
				.always(function () {
					submitBtn.removeAttr('disabled');
				});
		},

		validateForm: function (form) {
			var inputs = form.find('input'),
				valid = true;

			inputs.tooltip('destroy');

			$.each(inputs, function (index, val) {
				var input = $(val),
					val = input.val(),
					formGroup = input.parents('.form-group'),
					label = formGroup.find('label').text().toLowerCase(),
					textError = 'Введите ' + label;

				if (input.attr('type') == 'email' && !app.validateEmail(val)) {
					valid = false;
				}

				if (val.length === 0) {
					formGroup.addClass('has-error').removeClass('has-success');
					input.tooltip({
						trigger: 'manual',
						placement: 'left',
						title: textError
					}).tooltip('show');
					valid = false;
				} else {
					formGroup.addClass('has-success').removeClass('has-error');
				}
			});

			return valid;
		},

		actionInputKeydown: function (e) {
			app.removeError();

			var form = e.target.form;
			app.validateForm($(form));
		},

		validateEmail: function (val) {
			RegExpEmail = new RegExp(/.+@.+\..+/i);

			return RegExpEmail.test(val) == false ? false : true;
		},

		removeError: function () {
			$(this).tooltip('destroy').parents('.form-group').removeClass('has-error');
		},

		domReady: function () {

		}

	}

	$(document).ready(function () {
		app.initialize();
	});

	$(".age-division-block .title").on("click", function(){
		if ($(this).siblings(".person-list").hasClass("open")) {
			$(this).siblings(".person-list").removeClass("open").slideUp();
		} else {
			$(".person-list").removeClass("open").slideUp();
			$(this).siblings(".person-list").addClass("open").slideDown();
		};
	});

	$(window).scroll(function() {
		if ($(window).scrollTop() > 200) {
			$("header.fixed").addClass("visible");
		} else {
			$("header.fixed").removeClass("visible");
		};
	});

	//pop-up
	$(".showOrder").click(function(){
		$(".overlay-form").fadeIn();
		return false;
	});

	document.onkeydown = function(e) {
		if (e.keyCode === 27) { //escape
			$('.overlay-form').fadeOut();
		}
	};

	$("body").click(function(e) {
		if (e.target.className === "overlay-form") {
			$(".overlay-form").fadeOut();
		}
	});
	//end pop-up

}(jQuery));